// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The worm model

#include <curses.h>
#include "worm.h"
#include "board_model.h"
#include "worm_model.h"

// Data defining the worm
int theworm_wormpos_y[WORM_LENGTH];  // Array für : y-coordinaten of the worm
int theworm_wormpos_x[WORM_LENGTH];  // Array für : x-coordinaten of the worm
int theworm_maxindex;               // Last usable index into the arrays theworm_headpos_x and theworm_headpos_y
int theworm_headindex;              // an index into the array for the worm's head position 
                                    // 0 <= theworm_headindex <=theworm_maxindex // headindex liegt zwischen 0 und maxindex
// The current heading of the worm
// These are offsets from the set {-1,0,+1}
int theworm_dx;
int theworm_dy;

enum ColorPairs theworm_wcolor; 




// Initialize the worm
enum ResCodes initializeWorm(int len_max, int headpos_y, int headpos_x,enum WormHeading dir, enum ColorPairs color) {
   
    //local variables for loops
    int i;
    
    //Initialise last usable index to len_max -1 :theworm_maxindex
    theworm_maxindex = len_max -1;

    //Initialise headindex: theworm_headindex
    theworm_headindex = 0;

    //Mark all elements as unused in the arrays of positions
    //theworm_wormpos_y[] and theworm_wormpos_x[]
    //AS unused position in the array is marked with code UNUSED_POS_ELEM
    for(i=0;i< theworm_maxindex;i++){
    theworm_wormpos_x[i]=UNUSED_POS_ELEM;
    theworm_wormpos_y[i]=UNUSED_POS_ELEM;
    }

    // Initialize position of worms head
    theworm_wormpos_x[theworm_headindex] = 0; 
    theworm_wormpos_y[theworm_headindex] = getLastRow();

    // Initialize the heading of the worm
    setWormHeading(WORM_RIGHT);

    // Initialze color of the worm
    theworm_wcolor = color;

    return RES_OK;
}

// Show the worms's elements on the display
// Simple version
void showWorm() {
    // Due to our encoding we just need to show the head element
    // All other elements are already displayed
    placeItem(theworm_wormpos_y[theworm_headindex],theworm_wormpos_x[theworm_headindex],SYMBOL_WORM_INNER_ELEMENT,theworm_wcolor);
}

void cleanWormTail(){
    int tailindex;

      //Compute tailindex
      tailindex = (theworm_headindex+1)%theworm_maxindex;

      //Check the array of worm elements
      //is the array element at tailindex alrady in use?
      //Checking either array theworm_wormpos_y or theworm_wormpos_x is enough
      if(theworm_wormpos_y[tailindex]!=UNUSED_POS_ELEM){
      placeItem(theworm_wormpos_y[tailindex],theworm_wormpos_x[tailindex],SYMBOL_FREE_CELL,COLP_FREE_CELL);
      }
}

void moveWorm(enum GameStates* agame_state) {
    int headpos_x;
    int headpos_y;
    
    // Compute new head position according to current heading, but dont store it in the array yet
    headpos_x = theworm_wormpos_x[theworm_headindex] + theworm_dx;
    headpos_y = theworm_wormpos_y[theworm_headindex] + theworm_dy;

    // Check if we would leave the display if we move the worm's head according
    // to worm's last direction.
    // We are not allowed to leave the display's window.
    if (headpos_x < 0) {
        *agame_state = WORM_OUT_OF_BOUNDS;
    } else if (headpos_x > getLastCol() ) { 
        *agame_state = WORM_OUT_OF_BOUNDS;
    } else if (headpos_y < 0) {  
        *agame_state = WORM_OUT_OF_BOUNDS;
	} else if (headpos_y > getLastRow() ) {
        *agame_state = WORM_OUT_OF_BOUNDS;
    } else {
        // We will stay within bounds.
        // Check if the worm's head will collide with itself at new position
           if (isInUseByWorm(headpos_y,headpos_x)){
           //thats bad, stop game
           *agame_state = WORM_CROSSING;
           }
    }
    
    //Check the status of *agame_state
    //go on if nothing bad happend
    if(*agame_state == WORM_GAME_ONGOING){
    //so all is well : we did not hit anything bad and did not leave the window-> update the worm structure
    //Increment theworm_headindex
    //go round if end of the worm is reched (ring buffer)
    theworm_headindex = (theworm_headindex+1) % theworm_maxindex;
    //store new coordinates of head element in worm structure
    theworm_wormpos_x[theworm_headindex] = headpos_x;
    theworm_wormpos_y[theworm_headindex] = headpos_y;
    }

}

//a simple collision detection
bool isInUseByWorm(int new_headpos_y, int new_headpos_x){
    int i;
    bool collision = false;
    i = theworm_headindex;
    do{
        //Compare the position of the current worm element with the new_headpos
        if(theworm_wormpos_x[i] == new_headpos_x && theworm_wormpos_y[i] == new_headpos_y){
          collision = true;
        } 
        if (i==0)
        {
          i= theworm_maxindex;
        }
        else {
          i--;
        }
    }while( i != theworm_headindex && theworm_wormpos_x[i] != UNUSED_POS_ELEM);

    return collision;
}

// Setters
void setWormHeading(enum WormHeading dir) {
    switch(dir) {
        case WORM_UP :// User wants up
            theworm_dx=0;
            theworm_dy=-1;
            break;
        case WORM_DOWN :// User wants down
            theworm_dx=0;
            theworm_dy=1;
            break;
        case WORM_LEFT :// User wants left
            theworm_dx=-1;
            theworm_dy=0;
            break;
        case WORM_RIGHT :// User wants right
            theworm_dx=1;
            theworm_dy=0;
            break;
    }
} 

